#!/usr/bin/python3

import asyncio
import os
import sys
sys.path.insert(0, '/usr/share/bbscan')

from BBScan import main, parse_args  # Adicione parse_args aqui

if __name__ == '__main__':
    args = parse_args()

async def main():
    await asyncio.sleep(5.0)

if __name__ == '__main__':
    args = parse_args()
    print('* BBScan v2.0  https://github.com/lijiejie/BBScan *')
    if args.no_scripts:
        print('* Scripts scan was disabled')
    if args.require_ports:
        print('* Scripts scan port check: %s' % ','.join([str(x) for x in args.require_ports]))
    if sys.version_info.major >= 3 and sys.version_info.minor >= 10:
        loop = asyncio.new_event_loop()
    else:
        loop = asyncio.get_event_loop()
    q_targets = asyncio.Queue()    # targets Queue
    q_results = asyncio.Queue()    # results Queue
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
